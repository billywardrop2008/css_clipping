$(function() {
    console.log( "ready!" );
    $( ".vidbg-overlay" ).append( "        <svg class=\"video-svg\">\n" +
        "            <defs>\n" +
        "                <clipPath id=\"svgTextPath\">\n" +
        "                    <text x=\"50%\" y=\"50%\" dominant-baseline=\"middle\" text-anchor=\"middle\">LAMBENT</text>\n" +
        "                </clipPath>\n" +
        "            </defs>\n" +
        "        </svg>\n" +
        "        <div class=\"lambent-text\"></div>" );
});
